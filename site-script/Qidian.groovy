package qidian.site

import java.io.IOException
import java.nio.file.Files
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING
import org.springframework.beans.factory.annotation.Autowired
import org.xml.sax.SAXException
import groovy.json.JsonSlurper
import groovy.json.JsonParserType
import groovy.util.logging.Log4j2
import qidian.spring.Utility
/**
 * 起點網頁的運作方式記錄：透過 https://book.qidian.com/info/書號 從Cookie 取得_csrfToken的值
 * 然後https://book.qidian.com/ajax/book/category?_csrfToken=前面取值&bookId=書號 取得 JSON
 * data.vs[] 分卷，data.vs*.hS 決定該分卷是否為VIP卷，data.vs*.cs[] 章節
 * data.vs.cs*.id 章節編號, data.vs.cs*.cN 書名, data.vs.cs*.cU 加密章節編號
 * 然後可以透過目錄觀查到 VIP章節與刷費章節超連結的不同
 * 
 * @author Kent Yeh
 */
@Log4j2
class Qidian extends AbstractSite {
    def pBId = ~/(\d+)/
    def pCId = ~/chapter\/[^\/]+\/([^\/]+)$/  
    def pBCId = ~/m.qidian.com\/book\/(\d+)\/(\d+)/

    @Autowired private HtmlSlurper htmlSlurper
    
    def String getToc() {
         "https://m.qidian.com/book/${bookId}/catalog"
    }
    def int order = -1
    def anchorClosure = {it.name()=='a' && 'data-chapter-id' in it.attributes() && it.parent().name()=='li' && it.parent().attributes()['class']=='chapter-li jsChapter'}
     
    def https ={it.startsWith('//')?"https:${it}":"https://m.qidian.com/book/${bookId}/${it}"}
    
    def void run(){
        def errmsg = "取回${bookName}目錄(${getToc()})"
        def html = htmlSlurper.parse(getToc())
        try{
            def scripts = html.body.'**'.findAll {it.name()=='script'  && it.attributes().size()==0}
            for(def script :scripts){
                for(def line :script.text().split("\\r?\\n")){
                    if(line.contains('g_data.volumes')){
                        def data = line.substring(line.indexOf('['))
                        if(data.trim().endsWith(';')){
                            data = data.substring(0,data.size()-1);
                        }
                        def json = new JsonSlurper().setType(JsonParserType.CHAR_BUFFER).parseText(data)
                        def simple = folder.resolve("${bookName}_CN.txt")
                        for(def vol:json){
                            if(vol.hS){//VIP
                                break
                            }else{
                                for(def chp : vol.cs){
                                    if(alive &&  isStart(chp.cN)){
                                        def pageUrl = https(chp.id.toString())
                                        log.debug "開啟 ${pageUrl}"
                                        log.info "--${chp.cN}"
                                        errmsg = "取回${chp.cN}(${pageUrl})"
                                        def page = htmlSlurper.parse(pageUrl)
                                        def txt = ''<<''
                                        page.body.'**'.findAll {it.name()=='p' && it.parent().name()=='div' && it.parent().parent().attributes()['data-chapter-id']=="${chp.id}"}.each {
                                            txt << it.text() << br
                                        }
                                        if(txt.size()>0){
                                            try{
                                                def file = simple.toFile()
                                                def bom = file.length()==0?this.bom:'' 
                                                file << bom << '--' << chp.cN << br<< txt
                                            }catch(Exception ex){
                                                log.error ex.message,ex
                                                break;
                                            }
                                        }
                                    }
                                }
                            } 
                        }
                        if (alive && Files.exists(simple)) {
                            Utility.translate(simple, folder.resolve("${bookName}.txt"))
                            Files.delete(simple)
                        }
                    }
                }
            }
        }catch(Exception ex){
            log.error "${ex.message}",ex
        }
    }
    
    def String toString(){
        '起點中文'
    }
    def boolean isQualify(String url){
        if(url.contains('read.qidian.com/chapter/')){
            def html = htmlSlurper.parse(url)
            def keywords = html.head.'**'.find{it.name()=='meta' && it.attributes()['name']=='keywords'}.@content
            def val = keywords.text().split(',')
            bookName =  Utility.translate(val[0])
            startCaption =  val[1]
            bookId =html.body.'**'.find {it.name() == 'a' && it.attributes().containsKey('data-bid')}.'@data-bid'.text()
            return  bookId?.trim() && bookName?.trim() && startCaption?.trim()
        }else if(url.contains('qidian.com')){
            def m = url =~ pBCId
            if(m){
                bookId = m[0][1]
                def html = htmlSlurper.parse("https://m.qidian.com/book/${m[0][1]}/${m[0][2]}.html")
                def keywords = html.head.'**'.find{it.name()=='meta' && it.attributes()['name']=='keywords'}.@content
                def val = keywords.text().split(',')
                bookName =  Utility.translate(val[0])
                startCaption =  val[1]
                return  bookId?.trim() && bookName?.trim() && startCaption?.trim()
            }
            m = url =~ pBId
            if(m){
                bookId = m[0][1]
                def html = htmlSlurper.parse("https://book.qidian.com/info/${bookId}/")
                bookName= Utility.translate(html.head.'**'.find{it.name()=='meta' && it.attributes()['property']=='og:title'}.@content.text())
                startCaption = ''
                return  bookId?.trim() && bookName?.trim()
            }
        }
        false
    }
} 