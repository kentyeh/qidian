# 說明 #

本程式的主要功能是下載[起點](https://www.qidian.com/) 小說的公開版或是起點免費頻道版本的文字檔案(也能下載其它站點的小說，不過要有需要時才會開發)

### 一般人員使用方式 ###
  請到[這裡](https://www.mediafire.com/folder/ki51zr52kni76/qidian)下載編譯檔備用(須裝Java 8)，
  程式預設只會下載[起點](https://www.qidian.com/) 的執行腳本，若要使用其它網站，請到[這裡](http://www.mediafire.com/folder/r2jybia8bui7p/site-script)下載其它網站的 groovy 檔案到 qidian-3.0.0.jar 同目錄下的次目錄 site-script，然後重新啟動程式就可以了。

#### 執行程式 ####
* 以Java執行 qidian-3.0.0.jar

  ```
  java -jar qidian-3.0.0.jar
  ```

* 或是Windows 直接在檔案總管對qidian-3.0.0.jar按滑鼠右鍵選擇開啟


#### 使用方式 ####
把要開始下載的網址或是相關書頁的網址貼上後按解析，就會填入相關的參數，完成網址參數解析後，按下載，則開始進行下載(預設把小說文字檔下載到執行目錄下的次目錄 fiction)

### 程式人員使用方式 ###

* 首先設置好您的Java 8執行環境
* 安裝[Maven](http://maven.apache.org)
* 執行 `mvn package` 命令，打包程式並建立 qidian-version.number.jar
* 或是執行 `mvn -Pexec compile exec:java` 直接從程式碼執行，方便除錯

#### 輸入參數說明 ####
這一段本來說明支援的各個網站，但是後來發現世界變化的太快了，有的網站不是不見了(如卡提諾)，或是不再更新，還有就是經常改版(指[起點](https://www.qidian.com/))，所以決定重新改版，網站改用Groovy Script撰寫(放在qidian-site目錄下)，程式啟動時再載入，以便隨時能針對不同網站，隨時改寫而不用每次都要重新編譯並打包。

#### Groovy Script 開發說明 ####
1. Groovy Script檔案**必須以UTF-8編碼存檔**，且**不可以有BOM**(Byte Order Mark)
2. 類別須繼承 `AbstractSite`
3. 對Class標記 `@Log4j2` 可取得 [log](https://logging.apache.org/log4j/2.x/manual/customloglevels.html#ExampleUsage) 物件
4. 必須實做以下方法
    *  下拉網頁名稱的排序

        ```
        def int order = 排序整數
        ```

    *  下拉網頁的名稱顯示必須改寫

        ```
        def String toString(){'網站顯示名稱'}
        ```

    *  檢查網址(url)是否符合下載要求

        ```
        def boolean isQualify(String url){}
        ```

         如果符合下載要求則必須設定下列資訊

          1. `bookId = 書號`

          2. `bookName = 要儲存的書籍檔名`

          3. `startCaption = 下載起始章節`，如果從開始章節下載，則設為 ''

    * 執行下載

```
        def void run(){
           //首先取得目錄頁
           def catalog = ...
           //然後截取出所有書頁超連結與其章節文字
           def anchors = ...
           //開始迴圈下載網頁
           anchors.each {
            //如果 alive(表示主界面還未關閉) 而且 可以開始下載(符合下載章節)
             if(alive &&  isStart(it.text())){
               //下載該章節(folder是下載目錄所在)
               def downloadfile = folder.resolve("${bookName}_CN.txt").toFile()
               downloadfile << '輸出章節內容到檔案'
             }
           }

           //如有主界面還未關閉且必要則進行簡繁轉換(待轉換檔案,轉換目的檔案)
           if (alive && Files.exists(downloadfile)) {
             Utility.translate(downloadfile, folder.resolve("${bookName}.txt"))
             Files.delete(downloadfile) //轉換完成後刪除簡體檔
           }
        }

```

* 為了方便置換，預設了一個變數  nbsp ，實際上是 Pattern.compile("\u00a0"))，可直接用來置換或消除 &nbsp;，例如

```
  def m = node.text() =~ nbsp
  m.replaceAll(' ').trim() //真正的去除頭尾的空白
```

* 為了方便，預設了一個變數 folder，實際上是個 java.nio.file.Path ，指向 執行目錄下的次目錄 fiction，例如想要得到fitction目錄下的檔案 Path如

```
  def path = folder.resolve("${bookName}.txt")
  def file = simple.toFile()
```

* 為了方便，預設了一個變數 br，代表與系統相關的換行碼，在Windows 下是\r\n ，其它系統則是\n

```
  def txt = ''<< br
```

* 為了方便，預設了一個變數 bom表示 U+FEFF(位元組順序記號)，方便像Windows記事本辨識內文編碼

```
  def bom = file.length()==0?this.bom:'' 
  file << bom << '--章節'
```
  
#### Groovy Script 可注入物件說明 ####

* Groovy剖析HTML網頁的包裝，以取得GPathResult

  ```
  @Autowired private HtmlSlurper htmlSlurper
  ```

* Reactor Netty取得網頁的Library，主要採Connecction Pooling，可以重覆使用連線，以前我用用來配合 [xerces](https://xerces.apache.org/xerces2-j/javadocs/xerces2/org/apache/xerces/parsers/DOMParser.html) 的DOMParser,將網頁剖析為XML，以配合Xpath查找節點

  ```
  @Autowired reactor.netty.http.client.HttpClient httpClient
  ```

* 配合XML剖析用，前是是必須把網頁依XML解析為 org.w3c.dom.Document

  ```
  @Autowired private javax.xml.xpath.XPath xpath
  ```

* 注入Spring Context物件，應該是程式設計者要直接使用Spring 環境

  ```
  @Autowired private org.springframework.context.ApplicationContext ac;
  ```


### 設計說明 ###

首先可使用的網頁剖析器有兩種：

1.  HtmlSlurper，看到名字就應該可以想像其功能與[XmlSlurper](http://docs.groovy-lang.org/latest/html/gapi/groovy/util/XmlSlurper.html)差不多，只不過對象從XML換成了HTML，其基底乃是構築在[NekoHTML](http://nekohtml.sourceforge.net/)之上，由於透過[SAX](https://zh.wikipedia.org/zh-tw/SAX)走訪，對於結構簡單的網頁來說，程式碼變得異常簡潔，可以快速搞定一個網站。


2. 另外一種就是[DOMParser](https://xerces.apache.org/xerces2-j/javadocs/xerces2/org/apache/xerces/parsers/DOMParser.html)了(基底也是NekoHTML)；對於太複雜結構的網頁(如原本的卡提諾論壇或是[伊莉討論區](http://www.eyny.com/))只好動用XPath了，但這必須靠HttpClient取得串流，所以程式碼也較多也較雜，但也可以感受的到在三者中，其效率最好。

其實也不是沒有其它的方式，例如[JTidy](http://jtidy.sourceforge.net/)，號稱速度最快，但對網頁結構的正確性也高求最高，對這個近十年前的東西也只好捨棄了。

上述三種也不是能解所有網頁，例如[刺猬猫](https://www.ciweimao.com/)，網頁就是下載一堆JavaScript編碼，然後在前端解碼，唯二的對策就是：其一是瞭解其編解碼的方式(太累了)，其二是透過像[HtmlUnit](http://htmlunit.sourceforge.net/)這類的Headless Browser，以真實瀏覽器的方式載入，再透過XPath或是Css Selector找到想要的文字資料，但…再加入這一大模組划算嗎？想想還是換個網站下載好了。

### 其它說明 ###

我的習慣是下載檔案會加個`_CN.txt`字尾，下載中若是失敗，fiction下會有一個 書名_CN.txt的檔案，這就是下載的檔案，
它會一直抓檔然後累加，直到最後才轉成正體中文檔案 書名.txt。

也就是說失敗時，只要保留檔案 書名_CN.txt，然後從特定的章節繼續開始抓就可以了。
 