package qidian;

import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;
import org.apache.logging.log4j.LogManager;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import qidian.spring.ApplicationContext;
import qidian.ui.MainFrame;

/**
 *
 * @author Kent Yeh
 */
public class Main {
    
    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(Main.class);
    
    public static void main(String[] args) {
        AbstractApplicationContext context = new AnnotationConfigApplicationContext(ApplicationContext.class);
        context.registerShutdownHook();
        for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
            if ("Nimbus".equals(info.getName())) {
                try {
                    UIManager.setLookAndFeel(info.getClassName());
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
                    logger.error(ex.getMessage(), ex);
                }
                break;
            }
        }
        MainFrame mf = new MainFrame(context);
        mf.pack();
        mf.setVisible(true);
    }
}
