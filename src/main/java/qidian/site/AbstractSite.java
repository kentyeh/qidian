package qidian.site;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;
import javax.swing.JButton;
import javax.swing.JTextField;
import qidian.spring.SiteConfiguare;
import qidian.ui.Downloader;

/**
 *
 * @author Kent Yeh
 */
public abstract class AbstractSite implements Runnable, SiteConfiguare {
    
    protected static final Pattern NBSP = Pattern.compile("\u00a0");
    protected static final Pattern NBSPS = Pattern.compile("\u00a0+");
    protected static final String BR = System.lineSeparator();
    protected static final String BOM = "\ufeff";
    private String bookId = null;
    private String bookName = null;
    private String startCaption = "";
    boolean started = false;
    AtomicBoolean dying = new AtomicBoolean(false);
    private JButton[] controlBtns;
    private JTextField failedAt;
    
    protected Pattern getNbsp() {
        return NBSP;
    }

    protected Pattern getNbsps() {
        return NBSPS;
    }
    
    public static String getBr() {
        return BR;
    }
    
    public static String getBom() {
        return BOM;
    }

    /**
     * 重置設定值
     *
     * @param bookId 書號
     * @param bookName 書名
     * @param startCaption 起始章節或頁次
     */
    public void init(String bookId, String bookName, String startCaption) {
        setBookId(bookId);
        setBookName(bookName);
        setStartCaption(startCaption == null ? "" : startCaption.trim());
        started = false;
    }
    
    @Override
    public String getBookId() {
        return bookId;
    }
    
    public void setBookId(String bookId) {
        this.bookId = bookId;
    }
    
    @Override
    public String getBookName() {
        return bookName;
    }
    
    public void setBookName(String bookName) {
        this.bookName = bookName;
    }
    
    public String getStartCaption() {
        return startCaption == null ? "" : startCaption;
    }
    
    public void setStartCaption(String startCaption) {
        this.startCaption = startCaption;
    }

    /**
     * 檔案存放路徑，預設是 fiction，可用 -Dfolder= 設定
     *
     * @return
     */
    public Path getFolder() {
        String folder = System.getProperty("folder");
        Path path = folder == null || folder.isEmpty() ? Paths.get(".", "fiction") : Paths.get(".", folder);
        if (!Files.exists(path)) {
            try {
                Files.createDirectories(path);
                return path;
            } catch (IOException ex) {
                return Paths.get(".");
            }
        } else {
            return path;
        }
    }

    /**
     * 判斷是否開始下載
     *
     * @param caption
     * @return
     */
    @Override
    public boolean isStart(String caption) {
        if (started || getStartCaption().isEmpty()) {
            return true;
        } else if (caption != null && !caption.isEmpty()) {
            if (!started && startCaption.equals(caption.trim())) {
                started = true;
            }
            return started;
        } else {
            return true;
        }
    }
    
    public void setStarted(boolean started) {
        this.started = started;
    }
    
    @Override
    public boolean isAlive() {
        return !dying.get();
    }
    
    public void setAlive(boolean alive) {
        dying.set(!alive);
    }

    /**
     * 開始下載
     *
     * @param startAt 失敗章節
     * @param btns 控制按紐
     */
    public void startDownload(JTextField startAt, JButton... btns) {
        this.failedAt = startAt;
        this.controlBtns = btns;
        new Downloader(this).execute();
    }
    
    @Override
    public void failedAt(String chapter) {
        failedAt.setText(chapter);
        failedAt.requestFocus();
    }

    /**
     * 下載完成後叫用
     */
    @Override
    public void done() {
        for (JButton btn : this.controlBtns) {
            btn.setEnabled(true);
        }
    }
    
}
