package qidian.site;

import groovy.xml.XmlSlurper;
import groovy.xml.slurpersupport.GPathResult;
import io.netty.handler.codec.http.HttpResponseStatus;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.Duration;
import java.util.concurrent.CountDownLatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;
import qidian.spring.Utility;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;

/**
 * Groovy 以XML分析HTML的便利包
 *
 * @author Kent Yeh
 */
@Component("htmlSlurper")
public class HtmlSlurper {

    private org.springframework.context.ApplicationContext ctx;

    public ApplicationContext getCtx() {
        return ctx;
    }

    @Autowired
    public void setCtx(ApplicationContext ctx) {
        this.ctx = ctx;
    }

    /**
     * Groovy 以 XML 解析 HTML網頁
     *
     * @param url 網址
     * @param encoding 字元編碼名稱
     * @return
     */
    public GPathResult parse(final String url, final String encoding) {
        HttpClient httpClient = getCtx().getBean(HttpClient.class).headers(Utility::defaultHeader).followRedirect(true);
        CountDownLatch cdl = new CountDownLatch(1);
        return (GPathResult) httpClient.get().uri(url).responseSingle((res, buf) -> {
            if (res.status() == HttpResponseStatus.OK) {
                return buf.asInputStream();
            } else {
                return Mono.error(new RuntimeException(String.format(" 擷取網頁時發生錯誤:[%d]%s",
                        res.status().code(), res.status().reasonPhrase())));
            }
        }).map(is -> {
            try {
                return ctx.getBean(XmlSlurper.class, encoding).parse(new BufferedReader(new InputStreamReader(is, encoding)));
            } catch (SAXException | IOException ex) {
                return Mono.error(ex);
            } finally {
                try {
                    is.close();
                } catch (IOException ex) {

                }
            }
        }).doFinally(st -> {
            cdl.countDown();
        }).block(Duration.ofSeconds(10));
    }

    /**
     * Groovy 以 XML 解析 UTF-8 HTML網頁
     *
     * @param url 網址
     * @return
     * @throws IOException
     * @throws SAXException
     */
    public GPathResult parse(final String url) throws IOException, SAXException {
        return parse(url, "UTF-8");
    }

}
