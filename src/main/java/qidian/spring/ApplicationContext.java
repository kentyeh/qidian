package qidian.spring;

import java.io.InputStream;
import java.io.Reader;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import org.apache.xerces.parsers.DOMParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.cyberneko.html.HTMLConfiguration;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.InputSource;
import org.xml.sax.SAXNotRecognizedException;
import reactor.netty.http.client.HttpClient;

/**
 *
 * @author Kent Yeh
 */
@Lazy
@Configuration
@ComponentScan("qidian.site")
public class ApplicationContext implements InitializingBean, DisposableBean {

    private static final Logger logger = LogManager.getLogger(ApplicationContext.class);
    private final String httpHost = "qidian.com";
    public static final String USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64; rv:100.0) Gecko/20100101 Firefox/100.0";
    public static final int CONNECT_TIMEOUT = 5000;

    @Override
    public void afterPropertiesSet() throws Exception {
        System.setProperty("http.agent", USER_AGENT);
    }

    @Override
    public void destroy() throws Exception {
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_SINGLETON)
    public HttpClient httpClient() {
        return HttpClient.create();
    }

    @Bean
    public XPath xpath() {
        return XPathFactory.newInstance().newXPath();
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public InputSource inputSource(InputStream byteStream) {
        return new InputSource(byteStream);
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public InputSource inputSource(Reader characterStream) {
        return new InputSource(characterStream);
    }

    @Bean
    public HTMLConfiguration htmlConfiguration() {
        return new HTMLConfiguration();
    }

    /**
     *
     * @see  <a href="http://nekohtml.sourceforge.net/faq.html#uppercase">FAQ</a>
     * @param encoding 字元編碼
     * @return
     */
    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public DOMParser domParser(String... encoding) {
        DOMParser parser = new DOMParser(htmlConfiguration());
        try {
            parser.setFeature("http://xml.org/sax/features/namespaces", false);
            parser.setProperty("http://cyberneko.org/html/properties/default-encoding",
                    encoding == null || encoding.length == 0 ? "UTF-8" : encoding[0]);
            parser.setProperty("http://cyberneko.org/html/properties/names/elems", "lower");
            parser.setProperty("http://cyberneko.org/html/properties/names/attrs", "lower");
            parser.setFeature("http://cyberneko.org/html/features/scanner/ignore-specified-charset", true);
            parser.setFeature("http://cyberneko.org/html/features/override-doctype", false);
            parser.setFeature("http://cyberneko.org/html/features/override-namespaces", false);
            parser.setFeature("http://cyberneko.org/html/features/balance-tags", true);
        } catch (SAXNotRecognizedException | SAXNotSupportedException ex) {
            logger.error(ex.getMessage(), ex);
        }
        return parser;
    }

    /**
     * http://nekohtml.sourceforge.net/settings.html
     *
     * @param encoding 網頁編碼
     * @return
     */
    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public org.cyberneko.html.parsers.SAXParser saxParser(String encoding) {
        org.cyberneko.html.parsers.SAXParser parser = new org.cyberneko.html.parsers.SAXParser();
        try {
            parser.setProperty("http://cyberneko.org/html/properties/default-encoding", encoding);
            parser.setProperty("http://cyberneko.org/html/properties/names/elems", "lower");
            parser.setProperty("http://cyberneko.org/html/properties/names/attrs", "lower");
            parser.setFeature("http://cyberneko.org/html/features/scanner/ignore-specified-charset", true);
            parser.setFeature("http://cyberneko.org/html/features/override-doctype", false);
            parser.setFeature("http://cyberneko.org/html/features/override-namespaces", false);
            parser.setFeature("http://cyberneko.org/html/features/balance-tags", true);
        } catch (SAXNotRecognizedException | SAXNotSupportedException ex) {
            logger.error(ex.getMessage(), ex);
        }
        return parser;
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public groovy.xml.XmlSlurper xmlSlurper(String encoding) {
        return new groovy.xml.XmlSlurper(saxParser(encoding));
    }
}
