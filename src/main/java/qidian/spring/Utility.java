package qidian.spring;

import com.hankcs.algorithm.AhoCorasickDoubleArrayTrie;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpResponseStatus;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntConsumer;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import org.apache.logging.log4j.LogManager;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.netty.http.client.HttpClient;

/**
 *
 * @author Kent Yeh
 */
public class Utility {

    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(Utility.class);
    private static Map<String, String> wordmap;
    private static final String br = System.lineSeparator();
    private static AhoCorasickDoubleArrayTrie<String> acdat;

    /**
     * 取的聯結(Andchor)的文字
     *
     * @param xpath
     * @param node
     * @return
     * @throws javax.xml.xpath.XPathExpressionException
     */
    public static String getNodeText(XPath xpath, Node node) throws XPathExpressionException {
        XPathExpression expr = xpath.compile(".//text()");
        NodeList txtnodes = (NodeList) expr.evaluate(node, XPathConstants.NODESET);
        StringBuilder sb = new StringBuilder().append("");
        IntStream.range(0, txtnodes.getLength()).forEach(i -> {
            Optional<String> txt = Optional.ofNullable(txtnodes.item(i).getNodeValue());
            txt.filter(s -> !s.isEmpty()).ifPresent(src -> {
                if (i > 0) {
                    sb.append(br);
                }
                sb.append(src);
            });
        });
        return sb.toString();
    }

    public static void prepareSTMap() throws IOException {
        if (wordmap == null || wordmap.isEmpty() || acdat == null) {
            String resFolder = System.getProperty("resFolder");
            Path resPath = resFolder == null || resFolder.isEmpty() ? Paths.get(".", "site-script") : Paths.get(resFolder);
            downloadTSMapFiles();
            wordmap = new TreeMap();
            try ( BufferedReader brw = Files.newBufferedReader(resPath.resolve("word_s2t.txt"), StandardCharsets.UTF_8);  BufferedReader brp = Files.newBufferedReader(resPath.resolve("phrase_s2t.txt"), StandardCharsets.UTF_8)) {
                wordmap.put("\u00a0", "");
                for (String line; (line = brw.readLine()) != null;) {
                    String[] vals = line.split(",");
                    wordmap.put(vals[0], vals[1]);
                }
                Map<String, String> phrasemap = new TreeMap<>((String s1, String s2) -> s1.length() == s2.length() ? s1.compareTo(s2) : s1.length() < s2.length() ? Integer.MAX_VALUE : Integer.MIN_VALUE);
                for (String line; (line = brp.readLine()) != null;) {
                    String[] vals = line.split(",");
                    phrasemap.put(vals[0], vals[1]);
                }
                if (!phrasemap.isEmpty()) {
                    acdat = new AhoCorasickDoubleArrayTrie<>();
                    acdat.build(phrasemap);
                }
            }
        }
    }

    public static String translate(String simple) throws IOException {
        prepareSTMap();
        if (!wordmap.isEmpty() && acdat != null) {
            final AtomicInteger pos = new AtomicInteger(0);
            final StringBuilder sb = new StringBuilder();
            acdat.parseText(simple, (int start, int end, String phrase) -> {
                int idx = pos.get();
                if (idx == start) {
                    sb.append(phrase);
                    pos.set(end);
                } else if (idx < start) {
                    simple.substring(idx, start).codePoints().forEach(cp -> {
                        String w = new String(Character.toChars(cp));
                        w = Optional.ofNullable(wordmap.get(w)).orElse(w);
                        sb.append(w);
                    });
                    sb.append(phrase);
                    pos.set(end);
                }
            });
            if (pos.get() < simple.length()) {
                simple.substring(pos.get()).codePoints().forEach(cp -> {
                    String w = new String(Character.toChars(cp));
                    w = Optional.ofNullable(wordmap.get(w)).orElse(w);
                    sb.append(w);
                });
            }
            return sb.toString();
        } else {
            logger.error("缺少簡繁對照表！");
            return simple;
        }
    }

    public static void translate(Path simple, Path tradition) throws IOException {
        prepareSTMap();
        if (wordmap.isEmpty() || acdat == null) {
            logger.error("缺少簡繁對照表！");
            Files.copy(simple, tradition);
        } else {
            logger.debug("簡繁轉換中…");
            boolean started = false;
            AtomicBoolean error = new AtomicBoolean(false);
            StringBuilder src = new StringBuilder();
            try ( BufferedReader reader = Files.newBufferedReader(simple, StandardCharsets.UTF_8); final BufferedWriter writer = Files.newBufferedWriter(tradition, StandardCharsets.UTF_8,
                    StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)) {
                final AtomicInteger len = new AtomicInteger(0);
                IntConsumer cc = cp -> {
                    //<editor-fold defaultstate="collapsed" desc="實際轉換">
                    try {
                        if (cp == 10) {
                            writer.newLine();
                            len.set(0);
                        } else {
                            if (len.incrementAndGet() > 80) {
                                writer.newLine();
                                len.set(0);
                            }
                            String temp = new String(Character.toChars(cp));
                            writer.write(temp);
                        }
                    } catch (IOException ex) {
                        logger.error("轉換時IO發生錯誤：" + ex.getMessage(), ex);
                        error.set(true);
                    } finally {
                        src.setLength(0);
                    }
                    //</editor-fold>
                };
                for (String line; (line = reader.readLine()) != null;) {
                    if (error.get()) {
                        break;
                    }
                    if (started) {
                        src.append(br);
                    }
                    src.append(line);
                    if (src.length() > 4097) {
                        translate(src.toString()).codePoints().forEach(cc);
                    }
                    started = true;
                }
                if (!error.get() && src.length() > 0) {
                    translate(src.toString()).codePoints().forEach(cc);
                    logger.info("轉換完成！");
                }
            }
        }
    }

    public static HttpHeaders defaultHeader(HttpHeaders headers) {
        return defaultHeader(headers, false);
    }

    public static HttpHeaders defaultHeader(HttpHeaders headers, boolean urlencoded) {
        headers.add("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:100.0) Gecko/20100101 Firefox/100.0") //                .add("Cookie", "app=memcode%5F=4720796faa32bfcab5d181a902d61755&memid%5F=3599; ASPSESSIONIDCWATQQSQ=BBODMIJAKGKFADDCPNDAEOHO; cookiesession1=678B286FMNOQRSTUV01234678989B6EB'")
                ;
        if (urlencoded) {
            headers.add("Content-Type", "application/x-www-form-urlencoded");
        }
        return headers;
    }

    public static void downloadTxtfromJs(final Path save2, String... urls) {
        HttpClient httpClient = HttpClient.create().headers(Utility::defaultHeader);
        for (String url : urls) {
            CountDownLatch cdl = new CountDownLatch(1);
            httpClient.get().uri(url).responseSingle((res, buf) -> {
                if (res.status() == HttpResponseStatus.OK) {
                    return buf.asInputStream();
                } else {
                    return Mono.error(new RuntimeException(String.format(" 擷取網頁時發生錯誤:[%d]%s",
                            res.status().code(), res.status().reasonPhrase())));
                }
            }).map(is -> {
                boolean fileExists = Files.exists(save2);
                try ( BufferedWriter bw = Files.newBufferedWriter(save2, StandardCharsets.UTF_8,
                        StandardOpenOption.CREATE, StandardOpenOption.APPEND);  BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
                    Pattern dequote = Pattern.compile("[\",\\s]");
                    Pattern deColon = Pattern.compile(":");
                    for (String line; (line = br.readLine()) != null;) {
                        if (line.startsWith("//") || line.startsWith("var ") || line.startsWith("}")) {
                            //do not process
                        } else {
                            if (!fileExists) {
                                fileExists = true;
                            } else {
                                bw.newLine();
                            }
                            bw.write(deColon.matcher(dequote.matcher(line).replaceAll("")).replaceFirst(","));
                        }
                    }
                } catch (Exception ex) {
                    logger.error(String.format("下載%s失敗", save2.getFileName()), ex);
                } finally {
                    try {
                        is.close();
                    } catch (IOException ex) {
                    }
                }
                return Mono.empty();
            }).doFinally(st -> {
                cdl.countDown();
            }).subscribeOn(Schedulers.single()).subscribe(ps -> {
            }, ex -> {
                logger.error(String.format("下載" + save2.getFileName()) + "失敗:" + ex.getMessage(), ex);
            });
            try {
                cdl.await(10, TimeUnit.SECONDS);
            } catch (InterruptedException ex) {
                logger.error(" 等待網頁太久中斷:{}", url, ex.getMessage());
                break;
            }
        }
    }

    public static void downloadTSMapFiles() throws IOException {
        String resFolder = System.getProperty("resFolder");
        Path resPath = resFolder == null || resFolder.isEmpty() ? Paths.get(".", "site-script") : Paths.get(resFolder);
        if ((resFolder == null || resFolder.isEmpty()) && !Files.exists(resPath)) {
            Files.createDirectory(resPath);
        }
        Path tsp1 = resPath.resolve("word_s2t.txt");
        if (!Files.exists(tsp1)) {
            downloadTxtfromJs(tsp1, "https://raw.githubusercontent.com/softcup/New-Tongwentang-for-Firefox/master/src/lib/tongwen/tongwen_table_s2t.js",
                    "https://raw.githubusercontent.com/softcup/New-Tongwentang-for-Firefox/master/src/lib/tongwen/tongwen_table_ss2t.js");
        }
        Path tsp2 = resPath.resolve("phrase_s2t.txt");
        if (!Files.exists(tsp2)) {
            downloadTxtfromJs(tsp2, "https://raw.githubusercontent.com/softcup/New-Tongwentang-for-Firefox/master/src/lib/tongwen/tongwen_table_ps2t.js");
        }
    }
}
