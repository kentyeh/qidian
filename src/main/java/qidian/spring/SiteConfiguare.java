package qidian.spring;

/**
 * 各個小說網站的讀取設定
 *
 * @author Kent Yeh
 */
public interface SiteConfiguare {

    /**
     * 書號
     * @return 書號
     */
    public String getBookId();

    /**
     * 書名
     *
     * @return 書名
     */
    String getBookName();

    /**
     * 給定是否開始讀取的狀態.<br/>
     * 通常是針對起點外的網站，例如：前面80章已經看過，只要從81章處開始抓取， 所以必須給定開始抓取的狀態
     *
     * @param catpion 文章標題
     * @return 是否為起始下載章節
     */
    boolean isStart(String catpion);

    /**
     * 驗證是可以處理該網址
     *
     * @param url 待處理的網址
     * @return 網址是否合格
     */
    boolean isQualify(String url);

    /**
     * 程式是否還正常執行
     *
     * @return 
     */
    boolean isAlive();

    /**
     * 查驗是否符合網址之網站驗證順序
     *
     * @return 排序
     */
    int getOrder();

    void failedAt(String chapter);

    void done();
}
