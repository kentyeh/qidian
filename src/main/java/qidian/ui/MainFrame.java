package qidian.ui;

import groovy.lang.GroovyClassLoader;
import io.netty.handler.codec.http.HttpResponseStatus;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Label;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Stream;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import org.apache.logging.log4j.LogManager;
import org.codehaus.groovy.control.MultipleCompilationErrorsException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import qidian.site.AbstractSite;
import qidian.spring.SiteConfiguare;
import qidian.spring.Utility;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;

/**
 *
 * @author Kent Yeh
 */
public class MainFrame extends JFrame {

    private static final long serialVersionUID = -5141559605464762284L;
    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(MainFrame.class);
    public static final JTextArea MSGAREA = new JTextArea(20, 80);
    public static final List<AbstractSite> sites = new ArrayList<>();
    public static final Map<AbstractSite, String> siteMap = new HashMap<>();
    private final AbstractApplicationContext context;
    final JButton parseBtn = new JButton("解析");
    final JTextField urlField = new JTextField(30);

    public MainFrame(AbstractApplicationContext context) throws HeadlessException {
        this.context = context;
        Path siteGroovy = Paths.get(".", "site-script");
        if (!Files.exists(siteGroovy)) {
            try {
                siteGroovy = Files.createDirectory(siteGroovy);
            } catch (IOException ex) {
                logger.error("無法建立'{}'並下載Qidian.groovy", siteGroovy.toAbsolutePath().normalize().toString());
            }
        }
        if (Files.exists(siteGroovy)) {
            Path qidian = Paths.get(".", "site-script", "Qidian.groovy");
            if (!Files.exists(qidian)) {
                HttpClient httpClient = context.getBean(HttpClient.class).headers(Utility::defaultHeader);
                CountDownLatch cdl = new CountDownLatch(1);
                httpClient.get().uri("https://bitbucket.org/kentyeh/qidian/raw/master/site-script/Qidian.groovy").responseSingle((res, buf) -> {
                    if (res.status() == HttpResponseStatus.OK) {
                        return buf.asInputStream();
                    } else {
                        return Mono.error(new RuntimeException(String.format(" 擷取網頁時發生錯誤:[%d]%s",
                                res.status().code(), res.status().reasonPhrase())));
                    }
                }).map(is -> {
                    try {
                        Files.copy(is, qidian, StandardCopyOption.REPLACE_EXISTING);
                    } catch (IOException ex) {
                        logger.info("取得起點Script[Qidian.groovy]時發生錯誤", ex);
                        Mono.error(ex);
                    }
                    return Mono.empty();
                }).doFinally(st -> {
                    cdl.countDown();
                }).block(Duration.ofSeconds(10));
            }
            GroovyClassLoader loader = new GroovyClassLoader();
            try ( Stream<Path> walk = Files.walk(siteGroovy)) {
                walk.forEach(g -> {
                    if (g.toString().endsWith(".groovy")) {
                        try {
                            Class<SiteConfiguare> clazz = loader.parseClass(Files.newBufferedReader(g, StandardCharsets.UTF_8), g.getFileName().toString());
                            String fname = clazz.getSimpleName();
                            fname = Character.toLowerCase(fname.charAt(0)) + fname.substring(1);
                            if (siteMap.containsValue(fname)) {
                                int idx = 0;
                                while (siteMap.containsValue(String.format("%s%d", fname, ++idx))) {
                                }
                                fname = String.format("%s%d", fname, idx);
                            }
                            if (AbstractSite.class.isAssignableFrom(clazz)) {
                                if (ConfigurableApplicationContext.class.isAssignableFrom(context.getClass())) {
                                    BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(clazz);
                                    BeanDefinition beanDefinition = beanDefinitionBuilder.getRawBeanDefinition();
                                    context.getAutowireCapableBeanFactory().applyBeanPostProcessorsAfterInitialization(beanDefinition, fname);
                                    ((BeanDefinitionRegistry) ((ConfigurableApplicationContext) context).getBeanFactory())
                                            .registerBeanDefinition(fname, beanDefinition);
                                } else {
                                    AbstractSite gs = (AbstractSite) clazz.newInstance();
                                    sites.add(gs);
                                    siteMap.put(gs, fname);
                                }
                            }
                        } catch (IOException | IllegalAccessException | MultipleCompilationErrorsException | InstantiationException ex) {
                            logger.error("載入" + g.toAbsolutePath().normalize().toString() + "失敗：" + ex.getMessage(), ex);
                        }
                    }
                });
            } catch (IOException ex) {
                logger.error(ex.getMessage(), ex);
            }
        } else {
            logger.error("不存在{}", siteGroovy.toAbsolutePath().normalize().toString());
        }
        context.getBeansOfType(AbstractSite.class).entrySet().stream().forEach(entry -> {
            sites.add(entry.getValue());
            siteMap.put(entry.getValue(), entry.getKey());
        });
        Collections.sort(sites, (AbstractSite o1, AbstractSite o2) -> o1.getOrder() == o2.getOrder() ? 0 : o1.getOrder() > o2.getOrder() ? 1 : -1);

        super.setTitle("小說文字下載");
        initComponent();
    }

    private void initComponent() {
        FocusListener fl = new FocusListener() {

            @Override
            public void focusGained(FocusEvent e) {
                ((JTextField) e.getComponent()).selectAll();
            }

            @Override
            public void focusLost(FocusEvent e) {
            }
        };
        JPanel topPanel = new JPanel(new GridLayout(0, 1));

        JPanel parsePane = new JPanel();
        parsePane.setLayout(new BoxLayout(parsePane, BoxLayout.X_AXIS));
        topPanel.add(parsePane);

        parsePane.add(new Label("網址："));
        parsePane.add(urlField);
        parsePane.add(parseBtn);

        JPanel opPanel = new JPanel(new FlowLayout());
        topPanel.add(opPanel);
        opPanel.add(new Label("站別:"));
        final JComboBox<AbstractSite> siteCombo = new JComboBox<>(new SiteComboboxModel(sites));
        opPanel.add(siteCombo);
        siteCombo.setEnabled(true);
        siteCombo.setSelectedIndex(0);
        opPanel.add(new JLabel("書號:"));
        final JTextField bookIdField = new JTextField(10);
        bookIdField.addFocusListener(fl);
        opPanel.add(bookIdField);
        opPanel.add(new JLabel("書名:"));
        final JTextField bookNameField = new JTextField(15);
        bookNameField.addFocusListener(fl);
        opPanel.add(bookNameField);
        opPanel.add(new JLabel("起始章節:"));
        final JTextField startField = new JTextField(20);
        startField.addFocusListener(fl);
        opPanel.add(startField);
        final JButton downBtn = new JButton("下載");
        downBtn.registerKeyboardAction(downBtn.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false),
                JComponent.WHEN_FOCUSED);
        downBtn.registerKeyboardAction(downBtn.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
                JComponent.WHEN_FOCUSED);

        ActionListener validAction, dlAction;
        validAction = (ActionEvent e) -> {
            if (downBtn.isEnabled()) {
                final String url = urlField.getText();
                if (url.trim().isEmpty()) {
                    urlField.requestFocus();
                    JOptionPane.showMessageDialog(MainFrame.this, "請輸入網址",
                            "輸入錯誤", JOptionPane.ERROR_MESSAGE);
                } else {
                    new SwingWorker<Void, Void>() {

                        @Override
                        protected Void doInBackground() throws Exception {
                            parseBtn.setEnabled(false);
                            downBtn.setEnabled(false);
                            boolean parsed = false;
                            for (AbstractSite site : sites) {
                                try {
                                    if (site.isQualify(url)) {
                                        SwingUtilities.invokeLater(() -> {
                                            siteCombo.setSelectedItem(site);
                                            bookIdField.setText(site.getBookId());
                                            bookNameField.setText(site.getBookName());
                                            startField.setText(site.getStartCaption());
                                            logger.info("解析:{}", urlField.getText());
                                            urlField.setText("");
                                        });
                                        parsed = true;
                                        siteCombo.repaint();
                                        break;
                                    }
                                } catch (Exception ex) {
                                    logger.error("驗證" + site.toString() + "時錯誤:" + ex.getMessage(), ex);
                                }
                            }
                            if (!parsed) {
                                bookIdField.setText("");
                                bookNameField.setText("");
                                startField.setText("");
                                logger.error("解析網址失敗!!!");
                            }
                            return null;
                        }

                        @Override

                        protected void done() {
                            parseBtn.setEnabled(true);
                            downBtn.setEnabled(true);
                            downBtn.requestFocus();
                        }

                    }.execute();
                }
            } else {
                JOptionPane.showMessageDialog(MainFrame.this, "下載中…，完成後再進行網址解析");
            }
        };
        dlAction = (ActionEvent e) -> {
            Optional<String> bookid = Optional.ofNullable(bookIdField.getText());
            if (!bookid.filter(bid -> !bid.isEmpty()).isPresent()) {
                bookIdField.requestFocus();
                JOptionPane.showMessageDialog(MainFrame.this, "請輸入書號",
                        "輸入錯誤", JOptionPane.ERROR_MESSAGE);
                return;
            }
            Optional<String> bookName = Optional.ofNullable(bookNameField.getText());
            if (!bookName.filter(bname -> !bname.isEmpty()).isPresent()) {
                bookNameField.requestFocus();
                JOptionPane.showMessageDialog(MainFrame.this, "請輸入書名",
                        "輸入錯誤", JOptionPane.ERROR_MESSAGE);
                return;
            }
            String caption = startField.getText();
            Optional.ofNullable((AbstractSite) siteCombo.getSelectedItem()).ifPresent(site -> {
                downBtn.setEnabled(false);
                parseBtn.setEnabled(false);
                site.init(bookid.get(), bookName.get(), caption == null || caption.isEmpty() ? null : caption);
                site.startDownload(startField, downBtn, parseBtn);
            });
        };
        bookIdField.addActionListener(dlAction);
        bookNameField.addActionListener(dlAction);
        startField.addActionListener(dlAction);
        downBtn.addActionListener(dlAction);
        opPanel.add(downBtn);
        getContentPane().add(BorderLayout.NORTH, topPanel);
        JScrollPane scroller = new JScrollPane(MSGAREA);
        scroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        getContentPane().add(BorderLayout.CENTER, scroller);

        urlField.setMaximumSize(new Dimension(Integer.MAX_VALUE - 10, bookIdField.getPreferredSize().height));
        parseBtn.registerKeyboardAction(parseBtn.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false),
                JComponent.WHEN_FOCUSED);
        parseBtn.registerKeyboardAction(parseBtn.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
                JComponent.WHEN_FOCUSED);
        parseBtn.addActionListener(validAction);
        urlField.addActionListener(validAction);

        this.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                sites.forEach(site -> site.setAlive(false));
                MainFrame.this.context.close();
            }

            @Override
            public void windowActivated(WindowEvent e) {
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                Transferable content = clipboard.getContents(this);
                try {
                    Optional<String> url = Optional.ofNullable((String) content.getTransferData(DataFlavor.stringFlavor));
                    url.filter(href -> !href.isEmpty() && (href.toLowerCase().startsWith("http://")
                            || href.toLowerCase().startsWith("https://"))).ifPresent(href -> {
                        urlField.setText(href);
                        urlField.requestFocus();
                    });
                } catch (UnsupportedFlavorException | IOException ex) {
                    logger.error("拷貝剪貼薄發生錯誤:{}", ex.getMessage());
                }
            }

        });
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }
}
