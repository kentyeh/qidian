package qidian.ui;

import java.io.Serializable;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import javax.swing.SwingUtilities;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Core;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.appender.AppenderLoggingException;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.PatternLayout;

/**
 *
 * @author Kent Yeh
 */
@Plugin(name = "UIAppender", category = Core.CATEGORY_NAME, elementType = Appender.ELEMENT_TYPE, printObject = true)
public class UIAppenderImpl extends AbstractAppender {

    private static final long serialVersionUID = -1089086595733305812L;

    private final ReadWriteLock rwLock = new ReentrantReadWriteLock();
    private final Lock readLock = rwLock.readLock();

    public UIAppenderImpl(String name, Filter filter, Layout<? extends Serializable> layout, boolean ignoreExceptions) {
        super(name, filter, layout, ignoreExceptions);
    }

    @Override
    public void append(LogEvent event) {
        if (MainFrame.MSGAREA.isVisible()) {
            readLock.lock();
            try {
                final String prefix = event.getLevel().equals(Level.ERROR) || event.getLevel().equals(Level.FATAL) ? "錯誤：" : "";
                final byte[] bytes = getLayout().toByteArray(event);
                SwingUtilities.invokeLater(() -> {
                    MainFrame.MSGAREA.append(prefix + new String(bytes));
                });

            } catch (Exception ex) {
                if (!ignoreExceptions()) {
                    throw new AppenderLoggingException(ex);
                }
            } finally {
                readLock.unlock();
            }
        }
    }

    @PluginFactory
    public static UIAppenderImpl createAppender(
            @PluginAttribute("name") String name,
            @PluginElement("Layout") Layout<? extends Serializable> layout,
            @PluginElement("Filter") final Filter filter,
            @PluginAttribute("otherAttribute") String otherAttribute) {
        if (name == null) {
            LOGGER.error("No name provided for MyCustomAppenderImpl");
            return null;
        }
        layout = layout == null ? PatternLayout.createDefaultLayout() : layout;
        return new UIAppenderImpl(name, filter, layout, true);
    }
}
