package qidian.ui;

import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import qidian.site.AbstractSite;

/**
 *
 * @author Kent Yeh
 */
public class SiteComboboxModel extends AbstractListModel<AbstractSite> implements ComboBoxModel<AbstractSite> {

    private static final long serialVersionUID = -2666455494022001628L;
    List<AbstractSite> sites;
    AbstractSite selection = null;

    public SiteComboboxModel(List<AbstractSite> sites) {
        this.sites = sites;
    }

    @Override
    public int getSize() {
        return sites.size();
    }

    @Override
    public AbstractSite getElementAt(int index) {
        return sites.get(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        this.selection = (AbstractSite) anItem;
    }

    @Override
    public Object getSelectedItem() {
        return selection;
    }

}
