package qidian.ui;

import javax.swing.SwingWorker;
import qidian.site.AbstractSite;

/**
 *
 * @author Kent Yeh
 */
public class Downloader extends SwingWorker<Void, Void> {

    private AbstractSite site = null;

    public Downloader(AbstractSite runner) {
        this.site = runner;
    }

    @Override
    protected Void doInBackground() throws Exception {
        this.site.run();
        return null;
    }

    @Override
    protected void done() {
        super.done();
        site.done();
    }

}
